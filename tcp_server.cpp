#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <unistd.h> 
#include <string.h> 
#include <iostream> 
#include <sys/ipc.h> 
#include <sys/shm.h> 
#include <stdio.h> 
#include <time.h>
#include "opencv2/opencv.hpp"
#include "opencv2/imgcodecs.hpp"
#include "base64.h"
#include<vector>
#include <algorithm>
#include "json11.hpp"

#define PORT 3490
#define BACKLOG 10
using namespace std;
using namespace cv;

int main()
{
    struct sockaddr_in server;
    struct sockaddr_in dest;
    int status,socket_fd, client_fd,num;
    socklen_t size;

    char buffer[65535];
    string encoded_image;
    char *buff;
    int yes =1;
    if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0))== -1) {
        fprintf(stderr, "Socket failure!!\n");
        exit(1);
    }

    if (setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
        perror("setsockopt");
        exit(1);
    }
    memset(&server, 0, sizeof(server));
    memset(&dest,0,sizeof(dest));
    server.sin_family = AF_INET;
    server.sin_port = htons(PORT);
    server.sin_addr.s_addr = INADDR_ANY; 
    if ((bind(socket_fd, (struct sockaddr *)&server, sizeof(struct sockaddr )))== -1)    {
        fprintf(stderr, "Binding Failure\n");
        exit(1);
    }

    if ((listen(socket_fd, BACKLOG))== -1){
        fprintf(stderr, "Listening Failure\n");
        exit(1);
    }

    while(1) {
        size = sizeof(struct sockaddr_in);
        if ((client_fd = accept(socket_fd, (struct sockaddr *)&dest, &size))==-1 ) {
            perror("accept");
            exit(1);
        }
        printf("Server got connection from client %s\n", inet_ntoa(dest.sin_addr));

        int image=1;
        while(1) {

            if ((num = recv(client_fd, buffer,65535,0))== -1) {
                perror("recv ##");
                exit(1);
            }
            else if (num == 0) {
                printf("Connection closed\n");
                break;
            }
            buffer[num]='\0';
            string temp(buffer);
            cout<<"Input string is "<<temp<<endl;
            cout<<"=============================================\n\n\n";
            encoded_image += temp;
            if(strstr(buffer, "planys"))
            {
                std::string delimiter = "planys";
                size_t pos = 0;
                std::string token;
                while ((pos = encoded_image.find(delimiter)) != std::string::npos) {
                    token = encoded_image.substr(0, pos);
                    encoded_image.erase(0, pos + delimiter.length());
                }

                cout<<"First token is "<<token<<endl;
                cout<<"\n\n\n";
                cout<<"#####################################################################\n\n";
                cout<<"\n\n\n";
                // std::string err;
                // json11::Json data = json11::Json::parse(token, err);
                // if (!err.empty()) {
                //     return 1;
                // }
                // string rov_details = data["rov_details"].string_value();
                // cout<<"Rov details from server is "<<rov_details<<endl;
                // auto sim_details = data["sim_details"].string_value();
                // cout<<"Sim details from server is "<<sim_details<<endl;
                // string image = data["image"].string_value();
                // cout<<"Image Details are "<<image<<endl;
                // string time = data["time"].string_value();
                // cout<<"Time is "<<time<<endl;
                std::string msg_delimiter = "delimiter";
                size_t msg_pos=0;
                std::string msg_token;
                while ((msg_pos = token.find(msg_delimiter)) != std::string::npos) {
                    msg_token = token.substr(0, msg_pos);
                    cout<<"Msg token is "<<msg_token<<endl;
                    token.erase(0, msg_pos + msg_delimiter.length());
                }
                cout<<"Token is "<<token<<endl;
                string decoded_image = base64_decode(token);
                vector<uchar> vectordata(decoded_image.begin(),decoded_image.end());
                Mat image = imdecode(vectordata, IMREAD_UNCHANGED);
                size_t image_size = image.elemSize();
                imshow( "Frame_server ##$$@@!! ", image );
                waitKey(1);
                cout<<"======================================= \n\n\n";
            }
        } //End of Inner While...
        //Close Connection Socket
        close(client_fd);
    } //Outer While
    close(socket_fd);
    return 0;
} //End of main