#include "client_ws.hpp"
#include "server_ws.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h> 
#include <iostream> 
#include <sys/ipc.h> 
#include <sys/shm.h> 
#include <time.h>
#include "opencv2/opencv.hpp"
#include "opencv2/imgcodecs.hpp"
#include "base64.h"
#include "json11.hpp"
//#include "shared_memory.h"
//#include "Communicator.h"
#include <list>
#define MB 1024*1024
using namespace std; 
using namespace cv;

#define PORT 3490
#define MAXSIZE 1024

//std::list<Mat> frame_list;
Mat current_frame;
using WsServer = SimpleWeb::SocketServer<SimpleWeb::WS>;
using WsClient = SimpleWeb::SocketClient<SimpleWeb::WS>;

void* getImages(void* arg){

    while(1){
        VideoCapture cap("rtsp://admin:planysch4pn@192.168.1.64:554/Streaming/Channels/102/"); 
          // Check if camera opened successfully
        if(!cap.isOpened()){
            cout << "Error opening video stream or file" << endl;
            return (void *)-1;
        }

        Mat frame;
        cap >> frame;
        if (frame.empty())
            break;
        else
        {
            current_frame = frame;
        }
    }
}

int main() {
  // WebSocket (WS)-server at port 8080 using 1 thread

  pthread_t pImages;

  pthread_create(&pImages, NULL, &getImages, NULL);


  WsServer server;
  server.config.port = 8080;

  // Example 1: echo WebSocket endpoint
  // Added debug messages for example use of the callbacks
  // Test with the following JavaScript:
  //   var ws=new WebSocket("ws://localhost:8080/echo");
  //   ws.onmessage=function(evt){console.log(evt.data);};
  //   ws.send("test");
  auto &echo = server.endpoint["^/echo/?$"];

  echo.on_message = [&server](shared_ptr<WsServer::Connection> connection, shared_ptr<WsServer::Message> message) {
    auto message_str = message->string();

    //cout << "Server: Message received: \"" << message_str << "\" from " << connection.get() << endl;

    
            //     string token;
            //     token.clear();
            //   if(strstr(message_str.c_str(), "planys"))
            // {
            //     std::string delimiter = "planys";
            //     size_t pos = 0;
            //     while ((pos = message_str.find(delimiter)) != std::string::npos) {
            //         token = message_str.substr(0, pos);
            //         //cout<<"First part is "<<token<<endl;
            //         // cout<<"==============================\n\n";
            //         message_str.erase(0, pos + delimiter.length());
            //     }
                //cout<<"Second part is "<<message_str<<endl;
                //cout<<"======================================\n\n";

                std::string err;
                json11::Json data = json11::Json::parse(message_str, err);
                if (!err.empty()) {
                    //LOG_ERROR("Failed to parse JSON:  %s\n", err.c_str());
                    return;
                }

                  //cout<<"Data is "<<data.dump()<<endl;
                string rov_details = data["rov_details"].string_value();
                string sim_details = data["sim_details"].string_value();
                string image = data["image"].string_value();

                // cout<<"ROV Details are "<<rov_details<<endl;
                // cout<<"SIM Details are "<<sim_details<<endl;

                if(rov_details != "hello_msg" && sim_details != "hello_msg" && image != "hello_msg" && image != "no new image"){
                cout<<"ROV Details are "<<rov_details<<endl;
                cout<<"SIM Details are "<<sim_details<<endl;
                string decoded_image = base64_decode(image);
                vector<uchar> vectordata(decoded_image.begin(),decoded_image.end());
                Mat image = imdecode(vectordata, IMREAD_UNCHANGED);
                imshow( "Frame_server ##$$@@!! ", image );
                waitKey(1);
                }

                cout<<"========================================================================== \n\n";
            //}

    //cout << "Server: Sending message \"" << message_str << "\" to " << connection.get() << endl;

    auto send_stream = make_shared<WsServer::SendStream>();
    *send_stream << "received ---- from server";
    // connection->send is an asynchronous function
    connection->send(send_stream, [](const SimpleWeb::error_code &ec) {
      if(ec) {
        cout << "Server: Error sending message. " <<
            // See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
            "Error: " << ec << ", error message: " << ec.message() << endl;
      }
    });
    //server.stop();
    //server.connection_close(connection, echo,1002,"for_fun");
    //usleep(3000000);
  };

  echo.on_open = [](shared_ptr<WsServer::Connection> connection) {
    cout << "Server: Opened connection " << connection.get() << endl;
  };

  // See RFC 6455 7.4.1. for status codes
  echo.on_close = [](shared_ptr<WsServer::Connection> connection, int status, const string & /*reason*/) {
    cout << "Server: Closed connection " << connection.get() << " with status code " << status << endl;
  };

  // See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
  echo.on_error = [](shared_ptr<WsServer::Connection> connection, const SimpleWeb::error_code &ec) {
    cout << "Server: Error in connection " << connection.get() << ". "
         << "Error: " << ec << ", error message: " << ec.message() << endl;
  };

  // Example 2: Echo thrice
  // Demonstrating queuing of messages by sending a received message three times back to the client.
  // Concurrent send operations are automatically queued by the library.
  // Test with the following JavaScript:
  //   var ws=new WebSocket("ws://localhost:8080/echo_thrice");
  //   ws.onmessage=function(evt){console.log(evt.data);};
  //   ws.send("test");
  auto &echo_thrice = server.endpoint["^/echo_thrice/?$"];
  echo_thrice.on_message = [](shared_ptr<WsServer::Connection> connection, shared_ptr<WsServer::Message> message) {
    auto send_stream = make_shared<WsServer::SendStream>();
    *send_stream << message->string();

    connection->send(send_stream, [connection, send_stream](const SimpleWeb::error_code &ec) {
      if(!ec)
        connection->send(send_stream); // Sent after the first send operation is finished
    });
    connection->send(send_stream); // Most likely queued. Sent after the first send operation is finished.
  };

  echo_thrice.on_open = [](shared_ptr<WsServer::Connection> connection) {
    cout << "Server: Opened connection " << connection.get() << endl;
  };

  echo_thrice.on_close = [](shared_ptr<WsServer::Connection> connection, int status, const string & /*reason*/) {
    cout << "Server: Closed connection " << connection.get() << " with status code " << status << endl;
  };

  // Example 3: Echo to all WebSocket endpoints
  // Sending received messages to all connected clients
  // Test with the following JavaScript on more than one browser windows:
  //   var ws=new WebSocket("ws://localhost:8080/echo_all");
  //   ws.onmessage=function(evt){console.log(evt.data);};
  //   ws.send("test");
  auto &echo_all = server.endpoint["^/echo_all/?$"];
  echo_all.on_message = [&server](shared_ptr<WsServer::Connection> /*connection*/, shared_ptr<WsServer::Message> message) {
    auto send_stream = make_shared<WsServer::SendStream>();
    *send_stream << message->string();

    // echo_all.get_connections() can also be used to solely receive connections on this endpoint
    for(auto &a_connection : server.get_connections())
      a_connection->send(send_stream);
  };

  thread server_thread([&server]() {
    // Start WS-server
    server.start();
  });

  // Wait for server to start so that the client can connect
  this_thread::sleep_for(chrono::seconds(1));

  // Example 4: Client communication with server
  // Possible output:
  //   Server: Opened connection 0x7fcf21600380
  //   Client: Opened connection
  //   Client: Sending message: "Hello"
  //   Server: Message received: "Hello" from 0x7fcf21600380
  //   Server: Sending message "Hello" to 0x7fcf21600380
  //   Client: Message received: "Hello"
  //   Client: Sending close connection
  //   Server: Closed connection 0x7fcf21600380 with status code 1000
  //   Client: Closed connection with status code 1000
  WsClient client("localhost:8080/echo");
  client.on_message = [&client](shared_ptr<WsClient::Connection> connection, shared_ptr<WsClient::Message> message) {
    cout << "Client: Message received: \"" << message->string() << "\"" << endl;

       // while(1){

        Mat frame;
        std::string data;
        string rov_details = "rov_details";
            string sim_details = "sim_details";
            string message_image;
            frame = current_frame;
        if (frame.empty())
          {
            //usleep(2000000);
            message_image = "no new image";
          }
        else
        {
            
            //frame = frame_list.front();
            //frame_list.pop_front();

        std::vector<uchar> buffer;
        buffer.resize(200* MB);
        imencode(".png", frame, buffer);
        std::stringstream st(std::string(buffer.begin(), buffer.end()));
        std::string str(buffer.begin(), buffer.end());
        auto base64_png = reinterpret_cast<const unsigned char*>(buffer.data());
        string encoded_png = base64_encode(base64_png, buffer.size());
        message_image = encoded_png;
        
        }
        const json11::Json obj = json11::Json::object({
                    { "rov_details", rov_details},
                    { "sim_details", sim_details},
                    { "image", message_image},
                    });
        data = obj.dump();
    auto send_stream = make_shared<WsClient::SendStream>();
    *send_stream << data;
    connection->send(send_stream);
  
    //}
    //cout << "Client: Sending close connection" << endl;
    //connection->send_close(1000);
    //client.stop();
  };

  client.on_open = [&client](shared_ptr<WsClient::Connection> connection) {
    cout << "Client: Opened connection" << endl;
    string data;
    Mat frame;
        //std::string data;
        string rov_details = "hello_msg";
            string sim_details = "hello_msg";
            string message_image;
            frame = current_frame;
        if (frame.empty())
          {
            //usleep(2000000);
            message_image = "no new image";
          }
        else
        {
            
            //frame = frame_list.front();
            //frame_list.pop_front();

            std::vector<uchar> buffer;
        buffer.resize(200* MB);
        imencode(".png", frame, buffer);
        std::stringstream st(std::string(buffer.begin(), buffer.end()));
        std::string str(buffer.begin(), buffer.end());
        auto base64_png = reinterpret_cast<const unsigned char*>(buffer.data());
        string encoded_png = base64_encode(base64_png, buffer.size());
        message_image = encoded_png;
        
        }
     const json11::Json obj = json11::Json::object({
                    { "rov_details", rov_details},
                    { "sim_details", sim_details},
                    { "image", message_image},
                    });
        data = obj.dump();
    cout << "Client: Sending message: \"" << data << "\"" << endl;

    auto send_stream = make_shared<WsClient::SendStream>();
    *send_stream << data;
    connection->send(send_stream);
  };

  client.on_close = [](shared_ptr<WsClient::Connection> /*connection*/, int status, const string & /*reason*/) {
    cout << "Client: Closed connection with status code " << status << endl;
  };

  // See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
  client.on_error = [](shared_ptr<WsClient::Connection> /*connection*/, const SimpleWeb::error_code &ec) {
    cout << "Client: Error: " << ec << ", error message: " << ec.message() << endl;
  };

  client.start();


  //client.stop();
  //server.stop();
  server_thread.join();


  return 0;
}
