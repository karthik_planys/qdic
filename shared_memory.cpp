#include "shared_memory.h"

using namespace boost::interprocess;

bool SMemory::checkMemory(std::string variable)
{
  variable = "/dev/shm/"+variable;
  if (access(variable.c_str(), F_OK) == -1)
  {
    return false;
  }
  return true;
}

void SMemory::create_bool(std::string name)
{
  if (checkMemory(name))
    return;
  managed_shared_memory managed_shm{open_or_create, name.c_str(), 1024};
  if (managed_shm.find<bool>("bool").first)
  {
  }
  else
  {
    managed_shm.construct<bool>("bool")(false);
  }
}

void SMemory::create(std::string name,
                     unsigned int data_size)
{
  if (checkMemory(name))
    return;
  managed_shared_memory managed_shm{open_or_create, name.c_str(), 1024};
  if (managed_shm.find<double>("double").first)
  {
  }
  else
  {
    managed_shm.construct<double>("double")[data_size](0.0);
  }
}

void SMemory::create_str(std::string name)
{
  if (checkMemory(name))
    return;
  managed_shared_memory managed_shm{open_or_create, name.c_str(), 1024};
  typedef allocator<char, managed_shared_memory::segment_manager> CharAllocator;
  typedef basic_string<char, std::char_traits<char>, CharAllocator> string;
  if (managed_shm.find<string>("String").first)
  {
  }
  else
  {
    managed_shm.find_or_construct<string>("String")(
        "", managed_shm.get_segment_manager());
  }
}

bool SMemory::fetch_bool(std::string name)
{
  if (!checkMemory(name))
  {
    return false;
  }
  managed_shared_memory managed_shm{open_only, name.c_str()};
  std::pair<bool *, std::size_t> p = managed_shm.find<bool>("bool");
  if (p.first)
  {
    return *p.first;
  }
  else
  {
    return 0;
  }
}

double SMemory::fetch(std::string name, unsigned int arr_location)
{
  if (!checkMemory(name))
  {
    return 0; 
  }
  managed_shared_memory managed_shm{open_only, name.c_str()};
  std::pair<double *, std::size_t> p = managed_shm.find<double>("double");
  if (p.first)
  {
    return *(p.first + arr_location - 1);
  }
  else
  {
    return 0;
  }
}

std::string SMemory::fetch_str(std::string name)
{
  if (!checkMemory(name))
  {
    return "";
  }
  managed_shared_memory managed_shm{open_only, name.c_str()};
  typedef allocator<char, managed_shared_memory::segment_manager> CharAllocator;
  typedef basic_string<char, std::char_traits<char>, CharAllocator> string;
  std::pair<string *, std::size_t> s = managed_shm.find<string>("String");
  if (s.first)
  {
    std::string act_s(s.first->data(), s.first->size());
    return act_s;
  }
  else
  {
    return "";
  }
}

void SMemory::update_bool(std::string name, bool value)
{
  if (!checkMemory(name))
  {
    this->create_bool(name);    
  }
  managed_shared_memory managed_shm{open_only, name.c_str()};
  bool *i = managed_shm.find_or_construct<bool>("bool")();
  *i = value;
}

void SMemory::update(std::string name, unsigned int arr_location,
                     double value, unsigned int array_size)
{
  if (!checkMemory(name))
  {
    this->create(name, array_size);
    for(int i = 1; i <= array_size; i++)
    {
      this->update(name, i, 0, array_size);
    }    
  }
  if(arr_location > array_size)
  {
    return;
  }
  managed_shared_memory managed_shm{open_only, name.c_str()};
  double *i = managed_shm.find_or_construct<double>("double")();
  *(i + arr_location - 1) = value;
}

void SMemory::update_str(std::string name, std::string message)
{
  if (!checkMemory(name))
  {
    this->create_str(name);
  }
  managed_shared_memory managed_shm{open_only, name.c_str()};
  typedef allocator<char, managed_shared_memory::segment_manager> CharAllocator;
  typedef basic_string<char, std::char_traits<char>, CharAllocator> string;
  string *s = managed_shm.find_or_construct<string>("String")(
      "", managed_shm.get_segment_manager());
  s->assign(message.c_str());
}
