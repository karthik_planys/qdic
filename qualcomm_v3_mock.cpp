#include "client_ws.hpp"
#include "server_ws.hpp"
#include <string.h> 
#include <iostream>  
#include <sys/shm.h> 
#include "opencv2/opencv.hpp"
#include "opencv2/imgcodecs.hpp"
#include "base64.h"
#include "json11.hpp"
#include "shared_memory.h"
#include <algorithm>

// add an option for both beluga and mdl

#define MB 1024*1024
using namespace std; 
using namespace cv;

Mat current_frame;
string result, first_result;
std::string rov_result;
int only_once=0;

using WsServer = SimpleWeb::SocketServer<SimpleWeb::WS>;
using WsClient = SimpleWeb::SocketClient<SimpleWeb::WS>;

std::string to_string_with_precision(double value, const int n = 6)
{
    std::ostringstream out;
    out << std::fixed << std::setprecision(n) << value;
    return out.str();
}

void* getSharedMessageSetMock(void *arg)
{

    ifstream fin;
    string line;
    string delimiter = ",";

    //SMemory shmData;
    while(1)
    {

        string latitude_val;
        string longitude_val;
        size_t pos = 0;
        std::string token;
        bool flag=false;
        fin.open("lat_lon.csv");
        while(!fin.eof()){
            fin>>line;
        //cout<<"line is "<<line<<endl;
            while ((pos = line.find(delimiter)) != std::string::npos) {
                //token = line.substr(0, pos);
                latitude_val = line.substr(0,pos);
                //std::cout << token <<" ";
                line.erase(0, pos + delimiter.length());
                flag=true;
            }
            if(flag){
                //std::cout << line << std::endl;
                longitude_val = line;
                flag=false;
            }
        //}
        //rov_result.clear();
        //cout<<"From getSharedMessageSetMock \n\n";
        auto videoPath = "videoFolderPath";
        auto frontscreenshot_path = "frontScreenShotPath";
        auto bottomscreenshot_path = "bottomScreenShotPath";
        // if (frontscreenshot_path != "")
        // {
        //     shmData.update_str("frontScreenShotPath", "");
        // }
        // if (bottomscreenshot_path != "")
        // {
        //     shmData.update_str("bottomScreenShotPath", "");
        // }
        // auto latitude_val = "8";
        // auto longitude_val = "8";
        rov_result =
            "{\"sensor_data\":"
            "{\"frontCamFrameCount\":\"" +
            std::to_string(2) +
            "\"," + "\"bottomCamFrameCount\":\"" + std::to_string((double)(2)) +
            "\"," + "\"depth\":\"" +
            std::to_string(
                ((int)2) /
                1000.0) +
            "\"," + "\"altitude\":\"" +
            std::to_string(1) + "\"," +
            "\"thickness\":\"" + std::to_string(1) +
            "\"," + "\"heading\":\"" +
            std::to_string(1) +
            "\"," + "\"pitch\":\"" + std::to_string((2)) +
            "\"," + "\"roll\":\"" + std::to_string(3) +
            "\"," + "\"aclX\":\"" + std::to_string(4) + 
            "\"," + "\"aclY\":\"" + std::to_string(5) + 
            "\"," + "\"aclZ\":\"" + std::to_string(6) + 
            "\"," + "\"latitude\":\"" + latitude_val + "\"," + "\"longitude\":\"" +
            longitude_val + "\"," + "\"gpsAccuracy\":\"" +
            std::to_string(7) + "\"," +
            "\"gpsState\":\"" + std::to_string(8) +
            "\"," + "\"leak_sensor_1\":\"" +
            std::to_string(9) + "\"," +
            "\"leak_sensor_2\":\"" +
            std::to_string(10) + "\"," +
            "\"T_Depth\":\"" + std::to_string(11) +
            "\"," + "\"video_path\":\"" + std::to_string(12) +
            "\"," + "\"T_Heading\":\"" +
            std::to_string(13) + +"\"}," +
            "\"state_variable\":" +
            "{\"frontscreenshot_path\":\"" + frontscreenshot_path + "\"," +
            "\"bottomscreenshot_path\":\"" + bottomscreenshot_path + "\"}}";

            //this_thread::sleep_for(chrono::seconds(1));
            //cout<<"ROV result is "<<rov_result<<endl;

        }
            fin.close();
            line.clear();
    }
}

void* getImages(void* arg){

   // VideoCapture cap("/home/software/planys/Qualcomm/code/qualcomm_src/chaplin.mp4"); 

    while(1){
        //VideoCapture cap("rtsp://admin:planysch4pn@192.168.1.64:554/Streaming/Channels/102/");
        VideoCapture cap("rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov");
          // Check if camera opened successfully
        if(!cap.isOpened()){
            cout << "Error opening video stream or file" << endl;
            return (void *)-1;
        }
        Mat frame;
        cap >> frame;
        if (frame.empty())
            break;
        else{
          current_frame = frame;
          //imshow( "Image Capture !! ", frame );
         //waitKey(1);
        }
    }
}

string GetStdoutFromCommand(string cmd) {
  string data;
  FILE * stream;
  const int max_buffer = 1024;
  char buffer[max_buffer];
  cmd.append(" 2>&1");

  stream = popen(cmd.c_str(), "r");
  if (stream) {
    while (!feof(stream))
      if (fgets(buffer, max_buffer, stream) != NULL) data.append(buffer);
    pclose(stream);
  }
  return data;
}

string simResultV2(string command, string targets){

    string line;
    stringstream ss;

    ss.clear();
    line.clear();
    string value;
    ss<<command;
    int target_len = targets.size();
    while(std::getline(ss, line)){
        const char *line_str = line.c_str();
            const char* target = targets.c_str();
            const char* find = std::strstr(line_str, target);
            if(find != NULL){
                    value=line.substr(line.find(":")+1);
            }
    }
    return value;
}

void* getSIMValues(void* arg){

    vector<string> targets;

    while(1){

        //result.clear();

        string nas_signal_info = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 -p --nas-get-signal-info");
        string snr = "'9.4 dB'";
        snr.erase(remove(snr.begin(), snr.end(), '\''), snr.end());

        string qmi_dms_get_cap = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 -p --dms-get-capabilities");
        string max_tx_channel_rate = "'5000000'";
        max_tx_channel_rate.erase(remove(max_tx_channel_rate.begin(), max_tx_channel_rate.end(), '\''), max_tx_channel_rate.end());
        string max_rx_channel_rate = "'1000000'";
        max_rx_channel_rate.erase(remove(max_rx_channel_rate.begin(), max_rx_channel_rate.end(), '\''), max_rx_channel_rate.end());

        string qmi_dms_get_ids = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 -p --dms-get-ids");
        string imei = "'866758049837756'";
        imei.erase(remove(imei.begin(), imei.end(), '\''), imei.end());


        string qmi_dms_get_manufacturer = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 -p --dms-get-manufacturer");
        string manufacturer = "'QUALCOMM INCORPORATED'";
        manufacturer.erase(remove(manufacturer.begin(), manufacturer.end(), '\''), manufacturer.end());

        string qmi_dms_get_model = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 -p --dms-get-model");
        string model =  "'QUECTEL Mobile Broadband Module'";
        model.erase(remove(model.begin(), model.end(), '\''), model.end());

        string nas_get_home_network = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 -p --nas-get-home-network");
        string description = "'airtel'";
        description.erase(remove(description.begin(), description.end(), '\''), description.end());

        string nas_serving_system = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 -p --nas-get-serving-system");
        string selected_network = "'3gpp'";
        selected_network.erase(remove(selected_network.begin(), selected_network.end(), '\''), selected_network.end());

        string get_packet_service_status = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 -p --wds-get-packet-service-status");
        string connection_status = "'disconnected'";
        connection_status.erase(remove(connection_status.begin(), connection_status.end(), '\''), connection_status.end());

        string get_channel_rates = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 -p --wds-get-channel-rates");
        string current_tx_rate = "'0bps'";
        string current_rx_rate = "'0bps'";
        current_tx_rate.erase(remove(current_tx_rate.begin(), current_tx_rate.end(), '\''), current_tx_rate.end());
        current_rx_rate.erase(remove(current_rx_rate.begin(), current_rx_rate.end(), '\''), current_rx_rate.end());

        json11::Json obj=nullptr;

        if(only_once==0){
            obj = json11::Json::object({
                { "Max TX Channel Rate", max_tx_channel_rate},
                { "Max RX Channel Rate", max_rx_channel_rate},
                { "SINR", snr},
                { "IMEI", imei},
                { "Manufacturer", manufacturer},
                { "Model", model},
                { "Description", description},
                { "Selected network", selected_network},
                { "Connection status", connection_status},
                { "Current TX Rate", current_tx_rate },
                { "Current RX Rate", current_rx_rate },
                });
        }
        else{
            obj = json11::Json::object({
                { "SINR", snr},
                { "Selected network", selected_network},
                { "Connection status", connection_status},
                { "Current TX Rate", current_tx_rate },
                { "Current RX Rate", current_rx_rate },
                });

        }
        result = obj.dump();
        if(only_once==0) first_result=result;
        only_once=1;

        //this_thread::sleep_for(chrono::seconds(1));
        //cout<<"SIM data from thread is "<<result<<endl;
    }
}

string final_data(string rov_details, string sim_details){
    Mat frame;
    std::string data;
    string message_image;
    frame = current_frame;
    if (frame.empty()){
        message_image = "no new image";
    }
    else{
        std::vector<uchar> buffer;
        buffer.resize(200* MB);
        //cout<<"Frame is "<<frame<<endl;
        imencode(".png", frame, buffer);
        // cout<<"Buffer zero is "<<static_cast<unsigned>(buffer[0])<<endl;
        // cout<<"Buffer 100 is "<<static_cast<unsigned>(buffer[100])<<endl;
        std::stringstream st(std::string(buffer.begin(), buffer.end()));
        std::string str(buffer.begin(), buffer.end());
        auto base64_png = reinterpret_cast<const unsigned char*>(buffer.data());
        string encoded_png = base64_encode(base64_png, buffer.size());
        message_image = encoded_png;
    }
    std::time_t now= std::time(0);
    std::tm* now_tm= std::gmtime(&now);
    char buf[42];
    std::strftime(buf, 42, "%Y_%m_%d_%X", now_tm);
    //cout<<"Time "<<buf<<endl;
    const json11::Json obj = json11::Json::object({
                {"time", buf},
                { "rov_details", rov_details},
                { "sim_details", sim_details},
                { "image", message_image},
                });
    data = obj.dump();
        //cout<<"Final JSON Data is "<<data<<endl;
    return data;
}

int main(int argc, char* argv[]) {

    pthread_t pImages, pSIMValues, pROV;

    pthread_create(&pImages, NULL, &getImages, NULL);
    pthread_create(&pSIMValues, NULL, &getSIMValues, NULL);
    pthread_create(&pROV, NULL, &getSharedMessageSetMock, NULL);

    this_thread::sleep_for(chrono::seconds(1));

    // WsServer server;
    // server.config.port = 8080;

    // auto &echo = server.endpoint["^/echo/?$"];
    // echo.on_message = [&server](shared_ptr<WsServer::Connection> connection, shared_ptr<WsServer::Message> message) {
    //     auto message_str = message->string();
    //     std::string err;
    //     json11::Json data = json11::Json::parse(message_str, err);
    //     if (!err.empty()) {
    //         return;
    //     }
    //     string rov_details = data["rov_details"].string_value();
    //     auto sim_details = data["sim_details"].string_value();
    //     cout<<"Sim details from server is "<<sim_details<<endl;
    //     string image = data["image"].string_value();
    //     string time = data["time"].string_value();

    //     cout<<"ROV details are "<<rov_details<<endl;
    //     cout<<"Current time is "<<time<<endl;
    //     if(image != "hello_msg" && image != "no new image"){
    //         string decoded_image = base64_decode(image);
    //         vector<uchar> vectordata(decoded_image.begin(),decoded_image.end());
    //         Mat image = imdecode(vectordata, IMREAD_UNCHANGED);
    //         imshow( "Frame_server ##$$@@!! ", image );
    //         waitKey(1);
    //     }
    //     cout<<"==========================================================================\n\n";
    //     auto send_stream = make_shared<WsServer::SendStream>();
    //     *send_stream << "received ---- from server";
    //     connection->send(send_stream, [](const SimpleWeb::error_code &ec) {
    //       if(ec) {
    //         cout << "Server: Error sending message. " <<
    //             "Error: " << ec << ", error message: " << ec.message() << endl;
    //       }
    //     });
    // };

    // echo.on_open = [](shared_ptr<WsServer::Connection> connection) {
    //     cout << "Server: Opened connection " << connection.get() << endl;
    // };

    // echo.on_close = [](shared_ptr<WsServer::Connection> connection, int status, const string & /*reason*/) {
    //     cout << "Server: Closed connection " << connection.get() << " with status code " << status << endl;
    // };

    // echo.on_error = [](shared_ptr<WsServer::Connection> connection, const SimpleWeb::error_code &ec) {
    //     cout << "Server: Error in connection " << connection.get() << ". "
    //      << "Error: " << ec << ", error message: " << ec.message() << endl;
    // };

    // thread server_thread([&server]() {
    //     server.start();
    // });

    // this_thread::sleep_for(chrono::seconds(1));

    /*======================================CLIENT====================================================================*/

    int error_value;
    //WsClient client("ec2-13-233-236-231.ap-south-1.compute.amazonaws.com:8080");
    //WsClient client("192.168.1.136:8080");
        WsClient client(argv[1]);
    //WsClient client("127.0.0.1:8080/echo");
    client.on_message = [&client](shared_ptr<WsClient::Connection> connection, shared_ptr<WsClient::Message> message) {
        string data = final_data(rov_result, result);
        auto send_stream = make_shared<WsClient::SendStream>();
        *send_stream << data;
        //sleep(2);
        cout<<"Sending data is "<<data<<endl;
        connection->send(send_stream);
    };

    client.on_open = [&client](shared_ptr<WsClient::Connection> connection) {
        cout << "Client: Opened connection" << endl;
        string data = final_data(rov_result, first_result);

        auto send_stream = make_shared<WsClient::SendStream>();
        *send_stream << data;
        connection->send(send_stream);
    };

    client.on_close = [](shared_ptr<WsClient::Connection> , int status, const string & /*reason*/) {
        cout << "Client: Closed connection with status code " << status << endl;
    };

    client.on_error = [&](shared_ptr<WsClient::Connection> connection, const SimpleWeb::error_code &ec) {
        cout << "Client: Error: " << ec << ", error message: " << ec.message() << endl;
        error_value=ec.value();
    };

    while(true){
        if(error_value !=0 ) {client.stop(); sleep(3); client.start(); }
        else break;
    }

    client.start();
    return 0;
}
