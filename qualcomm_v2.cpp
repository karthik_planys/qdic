#include "client_ws.hpp"
#include "server_ws.hpp"
//#include <stdio.h>
//#include <stdlib.h>
//#include <unistd.h>
//#include <errno.h>
#include <string.h>
//#include <netdb.h>
// #include <sys/types.h>
// #include <sys/socket.h>
// #include <netinet/in.h>
// #include <arpa/inet.h> 
#include <iostream> 
//#include <sys/ipc.h> 
#include <sys/shm.h> 
//#include <time.h>
#include "opencv2/opencv.hpp"
#include "opencv2/imgcodecs.hpp"
#include "base64.h"
#include "json11.hpp"
#include "shared_memory.h"

#define MB 1024*1024
using namespace std; 
using namespace cv;

Mat current_frame;
string result, first_result;
std::string rov_result;
int only_once=0;

using WsServer = SimpleWeb::SocketServer<SimpleWeb::WS>;
using WsClient = SimpleWeb::SocketClient<SimpleWeb::WS>;

std::string to_string_with_precision(double value, const int n = 6)
{
    std::ostringstream out;
    out << std::fixed << std::setprecision(n) << value;
    return out.str();
}

void* getSharedMessageSet(void *arg)
{
    SMemory shmData;
    while(1)
    {
        rov_result.clear();
        auto videoPath = shmData.fetch_str("videoFolderPath");
        auto frontscreenshot_path = shmData.fetch_str("frontScreenShotPath");
        auto bottomscreenshot_path = shmData.fetch_str("bottomScreenShotPath");
        if (frontscreenshot_path != "")
        {
            shmData.update_str("frontScreenShotPath", "");
        }
        if (bottomscreenshot_path != "")
        {
            shmData.update_str("bottomScreenShotPath", "");
        }
        auto latitude_val = to_string_with_precision(shmData.fetch("GPSRov", 1), 8);
        auto longitude_val = to_string_with_precision(shmData.fetch("GPSRov", 2), 8);
        rov_result =
            "{\"sensor_data\":"
            "{\"frontCamFrameCount\":\"" +
            std::to_string((double)(shmData.fetch("frame_count", 1))) +
            "\"," + "\"bottomCamFrameCount\":\"" + std::to_string((double)(shmData.fetch("frame_count", 2))) +
            "\"," + "\"depth\":\"" +
            std::to_string(
                ((int)(shmData.fetch("PressureSensor", 1) * 1000)) /
                1000.0) +
            "\"," + "\"altitude\":\"" +
            std::to_string(shmData.fetch("Altimeter", 1)) + "\"," +
            "\"thickness\":\"" + std::to_string(shmData.fetch("Cygnus", 1)) +
            "\"," + "\"heading\":\"" +
            std::to_string(((int)(shmData.fetch("IMU", 1) * 1000)) / 1000.0) +
            "\"," + "\"pitch\":\"" + std::to_string(shmData.fetch("IMU", 2)) +
            "\"," + "\"roll\":\"" + std::to_string(shmData.fetch("IMU", 3)) +
            "\"," + "\"aclX\":\"" + std::to_string(shmData.fetch("IMU", 7)) + 
            "\"," + "\"aclY\":\"" + std::to_string(shmData.fetch("IMU", 8)) + 
            "\"," + "\"aclZ\":\"" + std::to_string(shmData.fetch("IMU", 9)) + 
            "\"," + "\"latitude\":\"" + latitude_val + "\"," + "\"longitude\":\"" +
            longitude_val + "\"," + "\"gpsAccuracy\":\"" +
            std::to_string(shmData.fetch("GPSRov", 3)) + "\"," +
            "\"gpsState\":\"" + std::to_string(shmData.fetch("GPSRov", 4)) +
            "\"," + "\"leak_sensor_1\":\"" +
            std::to_string(shmData.fetch("LeakSensor", 1)) + "\"," +
            "\"leak_sensor_2\":\"" +
            std::to_string(shmData.fetch("LeakSensor", 2)) + "\"," +
            "\"T_Depth\":\"" + std::to_string(shmData.fetch("Cockpit", 1)) +
            "\"," + "\"video_path\":\"" + shmData.fetch_str("videoFolderPath") +
            "\"," + "\"T_Heading\":\"" +
            std::to_string(shmData.fetch("Cockpit", 2)) + +"\"}," +
            "\"state_variable\":" +
            "{\"frontscreenshot_path\":\"" + frontscreenshot_path + "\"," +
            "\"bottomscreenshot_path\":\"" + bottomscreenshot_path + "\"} }\n" +
            "{\"video_path\":\"" + videoPath + "\"}";
    }
}

void* getImages(void* arg){

    while(1){
        VideoCapture cap("rtsp://admin:planysch4pn@192.168.1.64:554/Streaming/Channels/102/"); 
          // Check if camera opened successfully
        if(!cap.isOpened()){
            cout << "Error opening video stream or file" << endl;
            return (void *)-1;
        }
        Mat frame;
        cap >> frame;
        if (frame.empty())
            break;
        else{
          current_frame = frame;
        }
    }
}

string GetStdoutFromCommand(string cmd) {
  string data;
  FILE * stream;
  const int max_buffer = 1024;
  char buffer[max_buffer];
  cmd.append(" 2>&1");

  stream = popen(cmd.c_str(), "r");
  if (stream) {
    while (!feof(stream))
      if (fgets(buffer, max_buffer, stream) != NULL) data.append(buffer);
    pclose(stream);
  }
  return data;
}

void simResult(string command, vector<string> targets){

    string line;
    stringstream ss;

    ss.clear();
    line.clear();

    ss<<command;
    int target_len = targets.size();
    while(std::getline(ss, line)){
        const char *line_str = line.c_str();

        for(int i=0; i<target_len; i++){
            const char* target = targets[i].c_str();
            const char* find = std::strstr(line_str, target);
            if(find != NULL){
                string temp_target = target;
                if(temp_target == "Max TX channel rate" || temp_target == "Max RX channel rate")
                    result += temp_target + ":" + line.substr(line.find(":")+1) + "bps" + "\n";
                else
                    result += temp_target + ":" + line.substr(line.find(":")+1) + "\n";
            }
        }
    }
}

void* getSIMValues(void* arg){

    vector<string> targets;

    while(1){

        result.clear();
    
        string qmi_dms_get_cap = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 --dms-get-capabilities");
        if(only_once==0){
            targets.clear();
            targets.push_back("Max TX channel rate");
            targets.push_back("Max RX channel rate");
            simResult(qmi_dms_get_cap, targets);
        }

        string qmi_dms_get_ids = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 --dms-get-ids");
        if(only_once==0){
            targets.clear();
            targets.push_back("IMEI");
            simResult(qmi_dms_get_ids, targets);
        }

        string qmi_dms_get_manufacturer = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 --dms-get-manufacturer");
        if(only_once==0){
            targets.clear();
            targets.push_back("Manufacturer");
            simResult(qmi_dms_get_manufacturer, targets);
        }

        string qmi_dms_get_model = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 --dms-get-model");
        if(only_once==0){
            targets.clear();
            targets.push_back("Model");
            simResult(qmi_dms_get_model, targets);
        }

        string nas_get_home_network = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 --nas-get-home-network");
        if(only_once==0){
            targets.clear();
            targets.push_back("Description");
            simResult(nas_get_home_network, targets);
        }

        string nas_signal_info = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 --nas-get-signal-info");
        targets.clear();
        targets.push_back("SNR");
        simResult(nas_signal_info, targets);

        string nas_serving_system = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 --nas-get-serving-system");
        targets.clear();
        targets.push_back("Selected network");
        simResult(nas_serving_system, targets);

        string get_packet_service_status = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 --wds-get-packet-service-status");
        targets.clear();
        targets.push_back("Connection status");
        simResult(get_packet_service_status, targets);

        string get_channel_rates = GetStdoutFromCommand("qmicli --device=/dev/cdc-wdm0 --wds-get-channel-rates");
        targets.clear();
        targets.push_back("Current TX rate");
        targets.push_back("Current RX rate");
        simResult(get_channel_rates, targets);

        

        if(only_once==0) first_result=result;
        only_once=1;

        this_thread::sleep_for(chrono::seconds(1));


    }
}

string final_data(string rov_details_, string sim_details_){
    Mat frame;
    std::string data;
    string rov_details = rov_details_;
    string sim_details = sim_details_;
    string message_image;
    frame = current_frame;
    if (frame.empty()){
        message_image = "no new image";
    }
    else{
        std::vector<uchar> buffer;
        buffer.resize(200* MB);
        imencode(".png", frame, buffer);
        std::stringstream st(std::string(buffer.begin(), buffer.end()));
        std::string str(buffer.begin(), buffer.end());
        auto base64_png = reinterpret_cast<const unsigned char*>(buffer.data());
        string encoded_png = base64_encode(base64_png, buffer.size());
        message_image = encoded_png;
    }
    std::time_t now= std::time(0);
    std::tm* now_tm= std::gmtime(&now);
    char buf[42];
    std::strftime(buf, 42, "%Y_%m_%d_%X", now_tm);
    const json11::Json obj = json11::Json::object({
                {"time", buf},
                { "rov_details", rov_details},
                { "sim_details", sim_details},
                { "image", message_image},
                });
    data = obj.dump();
    return data;
}

int main() {

    pthread_t pImages, pSIMValues, pROV;

    pthread_create(&pROV, NULL, &getSharedMessageSet, NULL);
    pthread_create(&pImages, NULL, &getImages, NULL);
    pthread_create(&pSIMValues, NULL, &getSIMValues, NULL);

    this_thread::sleep_for(chrono::seconds(1));

    WsServer server;
    server.config.port = 8080;

    auto &echo = server.endpoint["^/echo/?$"];
    echo.on_message = [&server](shared_ptr<WsServer::Connection> connection, shared_ptr<WsServer::Message> message) {
        auto message_str = message->string();
        //cout<<"Message str from server is $$$$$$$$$ "<<message_str<<endl;
        std::string err;
        json11::Json data = json11::Json::parse(message_str, err);
        if (!err.empty()) {
            return;
        }
        string rov_details = data["rov_details"].string_value();
        string sim_details = data["sim_details"].string_value();
        string image = data["image"].string_value();
        string time = data["time"].string_value();

        
        cout<<"SIM details are "<<sim_details<<endl;
        cout<<"ROV details are "<<rov_details<<endl;
        cout<<"Current time is "<<time<<endl;
        if(image != "hello_msg" && image != "no new image"){
            string decoded_image = base64_decode(image);
            vector<uchar> vectordata(decoded_image.begin(),decoded_image.end());
            Mat image = imdecode(vectordata, IMREAD_UNCHANGED);
            imshow( "Frame_server ##$$@@!! ", image );
            waitKey(1);
        }
        cout<<"==========================================================================\n\n";
        auto send_stream = make_shared<WsServer::SendStream>();
        *send_stream << "received ---- from server";
        connection->send(send_stream, [](const SimpleWeb::error_code &ec) {
          if(ec) {
            cout << "Server: Error sending message. " <<
                "Error: " << ec << ", error message: " << ec.message() << endl;
          }
        });
    };

    echo.on_open = [](shared_ptr<WsServer::Connection> connection) {
        cout << "Server: Opened connection " << connection.get() << endl;
    };

    echo.on_close = [](shared_ptr<WsServer::Connection> connection, int status, const string & /*reason*/) {
        cout << "Server: Closed connection " << connection.get() << " with status code " << status << endl;
    };

    echo.on_error = [](shared_ptr<WsServer::Connection> connection, const SimpleWeb::error_code &ec) {
        cout << "Server: Error in connection " << connection.get() << ". "
         << "Error: " << ec << ", error message: " << ec.message() << endl;
    };

    thread server_thread([&server]() {
        server.start();
    });

    this_thread::sleep_for(chrono::seconds(1));

    /*======================================CLIENT====================================================================*/

    WsClient client("localhost:8080/echo");
    client.on_message = [&client](shared_ptr<WsClient::Connection> connection, shared_ptr<WsClient::Message> message) {
        //cout<<"SIM values in client is "<<result<<endl;
        string data = final_data("rov_details", result);
        auto send_stream = make_shared<WsClient::SendStream>();
        *send_stream << data;
        connection->send(send_stream);
    };

    client.on_open = [&client](shared_ptr<WsClient::Connection> connection) {
        cout << "Client: Opened connection" << endl;
       // cout << "SIM result in open connection is "<<first_result<<endl;
        string data = final_data("hello_msg", first_result);

        auto send_stream = make_shared<WsClient::SendStream>();
        *send_stream << data;
        connection->send(send_stream);
    };

    client.on_close = [](shared_ptr<WsClient::Connection> , int status, const string & /*reason*/) {
        cout << "Client: Closed connection with status code " << status << endl;
    };

    client.on_error = [](shared_ptr<WsClient::Connection> , const SimpleWeb::error_code &ec) {
        cout << "Client: Error: " << ec << ", error message: " << ec.message() << endl;
    };

    client.start();
    server_thread.join();
    return 0;
}
