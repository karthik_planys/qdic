void* getSharedMessageSet(void *arg)
{
    SMemory shmData;
    ifstream fin;
    string line;
    string delimiter = ",";

    while(1)
    {
        string latitude_val;
        string longitude_val;
        size_t pos = 0;
        std::string token;
        bool flag=false;
        fin.open("lat_lon.csv");

        while(!fin.eof()){
            fin>>line;
            while ((pos = line.find(delimiter)) != std::string::npos) {
                latitude_val = line.substr(0,pos);
                line.erase(0, pos + delimiter.length());
                flag=true;
            }
            if(flag){
                longitude_val = line;
                flag=false;
            }

            //rov_result.clear();
            auto videoPath = shmData.fetch_str("videoFolderPath");
            auto frontscreenshot_path = shmData.fetch_str("frontScreenShotPath");
            auto bottomscreenshot_path = shmData.fetch_str("bottomScreenShotPath");
            if (frontscreenshot_path != "")
            {
                shmData.update_str("frontScreenShotPath", "");
            }
            if (bottomscreenshot_path != "")
            {
                shmData.update_str("bottomScreenShotPath", "");
            }
            //auto latitude_val = to_string_with_precision(shmData.fetch("GPSRov", 1), 8);
            //auto longitude_val = to_string_with_precision(shmData.fetch("GPSRov", 2), 8);
            rov_result =
                "{\"sensor_data\":"
                "{\"frontCamFrameCount\":\"" +
                std::to_string((double)(shmData.fetch("frame_count", 1))) +
                "\"," + "\"bottomCamFrameCount\":\"" + std::to_string((double)(shmData.fetch("frame_count", 2))) +
                "\"," + "\"depth\":\"" +
                std::to_string(
                    ((int)(shmData.fetch("PressureSensor", 1) * 1000)) /
                    1000.0) +
                "\"," + "\"altitude\":\"" +
                std::to_string(shmData.fetch("Altimeter", 1)) + "\"," +
                "\"thickness\":\"" + std::to_string(shmData.fetch("Cygnus", 1)) +
                "\"," + "\"heading\":\"" +
                std::to_string(((int)(shmData.fetch("IMU", 1) * 1000)) / 1000.0) +
                "\"," + "\"pitch\":\"" + std::to_string(shmData.fetch("IMU", 2)) +
                "\"," + "\"roll\":\"" + std::to_string(shmData.fetch("IMU", 3)) +
                "\"," + "\"aclX\":\"" + std::to_string(shmData.fetch("IMU", 7)) + 
                "\"," + "\"aclY\":\"" + std::to_string(shmData.fetch("IMU", 8)) + 
                "\"," + "\"aclZ\":\"" + std::to_string(shmData.fetch("IMU", 9)) + 
                "\"," + "\"latitude\":\"" + latitude_val + "\"," + "\"longitude\":\"" +
                longitude_val + "\"," + "\"gpsAccuracy\":\"" +
                std::to_string(shmData.fetch("GPSRov", 3)) + "\"," +
                "\"gpsState\":\"" + std::to_string(shmData.fetch("GPSRov", 4)) +
                "\"," + "\"leak_sensor_1\":\"" +
                std::to_string(shmData.fetch("LeakSensor", 1)) + "\"," +
                "\"leak_sensor_2\":\"" +
                std::to_string(shmData.fetch("LeakSensor", 2)) + "\"," +
                "\"T_Depth\":\"" + std::to_string(shmData.fetch("Cockpit", 1)) +
                "\"," + "\"video_path\":\"" + shmData.fetch_str("videoFolderPath") +
                "\"," + "\"T_Heading\":\"" +
                std::to_string(shmData.fetch("Cockpit", 2)) + +"\"}," +
                "\"state_variable\":" +
                "{\"frontscreenshot_path\":\"" + frontscreenshot_path + "\"," +
                "\"bottomscreenshot_path\":\"" + bottomscreenshot_path + "\"} }\n" +
                "{\"video_path\":\"" + videoPath + "\"}";
                //cout<<"ROV result is "<<rov_result<<endl;
                //this_thread::sleep_for(chrono::seconds(1));

            }
            fin.close();
            line.clear();

    }
}