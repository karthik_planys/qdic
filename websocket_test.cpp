 #include "client_ws.hpp"
#include "server_ws.hpp"
#include <string.h> 
#include <iostream>  

using namespace std;

using WsClient = SimpleWeb::SocketClient<SimpleWeb::WS>;

 int main() {


 int error_value;
    //WsClient client("ec2-13-233-236-231.ap-south-1.compute.amazonaws.com:8080");
    //cout<<"Connecting WSCLIENT "<<endl;
    WsClient client("13.233.236.231:8080");
    //cout<<"Client connected "<<endl;
    //WsClient client("127.0.0.1:8080/echo");

    //cout<<"On message "<<endl;

    client.on_message = [&client](shared_ptr<WsClient::Connection> connection, shared_ptr<WsClient::Message> message) {
        cout<<"message from server is "<<message<<endl;
        auto send_stream = make_shared<WsClient::SendStream>();
        *send_stream << message;
        connection->send(send_stream);
    };

    //cout<<"Opening client "<<endl;
    
    client.on_open = [&client](shared_ptr<WsClient::Connection> connection) {
        cout << "Client: Opened connection" << endl;
        string data = "message from client ";
        auto send_stream = make_shared<WsClient::SendStream>();
        *send_stream << data;
        connection->send(send_stream);
    };

    
    //cout<<" On close "<<endl;
    client.on_close = [](shared_ptr<WsClient::Connection> , int status, const string & /*reason*/) {
        cout << "Client: Closed connection with status code " << status << endl;
    };

    //cout<<"On error "<<endl;

    client.on_error = [&](shared_ptr<WsClient::Connection> connection, const SimpleWeb::error_code &ec) {
        cout << "Client: Error: " << ec << ", error message: " << ec.message() << endl;
        error_value=ec.value();
        cout<<"ec error value "<<error_value<<endl;
    };

    cout<<"Starting client "<<endl;

    client.start();

    cout<<"client started "<<endl;

}