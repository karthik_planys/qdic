#!/bin/sh

ip rule del from 192.168.1.0/24 table qualcomm
ip rule add from 192.168.1.0/24 table qualcomm

ip rule del to 192.168.1.0/24 table qualcomm
ip rule add to 192.168.1.0/24 table qualcomm

ip route del 192.168.1.1 dev $1 table qualcomm
ip route add 192.168.1.1 dev $1 table qualcomm

ip route del default via 192.168.1.1 table qualcomm dev $1
ip route add default via 192.168.1.1 table qualcomm dev $1


route del -net 0.0.0.0 gw 192.168.1.1 netmask 0.0.0.0 dev $1

route del -net 0.0.0.0 gw 10.64.64.64 netmask 0.0.0.0 dev ppp0
route add -net 0.0.0.0 gw 10.64.64.64 netmask 0.0.0.0 dev ppp0
